"""
v0.0.2
"""
import os
import sqlite3

conn = sqlite3.connect(os.path.dirname(os.path.abspath(__file__)) + '/ip.db')
c = conn.cursor()

c.execute('CREATE TABLE ip (ip TEXT UNIQUE, domain TEXT, last_seen TIMESTAMP)')

conn.commit()
conn.close()