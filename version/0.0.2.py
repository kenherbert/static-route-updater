"""
Upgrade from v0.0.1 to v0.0.2
"""
import os
import sqlite3

conn = sqlite3.connect('ip.db')
c = conn.cursor()

c.execute('ALTER TABLE ip ADD COLUMN domain TEXT')

conn.commit()
conn.close()