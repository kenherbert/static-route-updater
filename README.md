# Static route updater

Resolves a series of domain names to IP addresses, then configures static routes for all IPs seen in the past 4 weeks (or other configurable period). Should work on any platform that uses the `networksetup` command (tested on MacOS Mojave).




## Dependencies
### Python 3.6 or greater
Verified working with Python 3.7.4.




### dnspython 2.0.0 or greater
The script uses dnspython to resolve the domain name.

Just run `pip install dnspython` and you should be good to go.




## Usage

Edit the following in `main.py`:
- Change the `URLS` list to whatever domains you need
- Set the `PERIOD` (default is 4 weeks) - actual value is in seconds

- Set the `SERVICE_NAME` (default is 'Wi-Fi') - if you don't know your service name you can find all network services with `networksetup -listallnetworkservices` - I can't tell you which you are actually using but the default value is a pretty good bet
- Set your `MASK` and `GATEWAY` if you need to (the default values are pretty standard)



Then set yourself up a cron job to run `main.py` at some regular interval (likely will need to be run as a privileged user eg. root).



A cron job to run the script every hour would look something like:
```cron
0 * * * * <path/to/python> <path/to/script>/main.py >> /var/log/static-route-updater.log 2>&1
```



Depending on how high- or low-priority updates to the rule are for you, and how you use your computer, you should choose intervals/times that best fit your needs. Eg. running it every day at midnight if your computer is powered down at that time won't do you much good, while running it hourly for a site you use every few months could be overkill!



Note that if no IPs for the defined domains have been resolved within the last period your static routes will be cleared.




Logging is minimal (just a success message if the end of the script is reached without error), so anything else output to a log file (like in my cron example above) will just be errors thrown by the script if anything comes up.
