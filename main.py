import os
import sqlite3
import dns.resolver
from datetime import datetime
import subprocess

# Begin user configurable data
URLS = [
    'ads-serve.brave.com',
    'publish0x.com'
]

PERIOD = 60 * 60 * 24 * 7 * 4 #4 weeks

SERVICE_NAME = 'Wi-Fi'
MASK = '255.255.255.0'
GATEWAY = '192.168.0.1'
# End user configurable data



# Resolve IPs and update the database
conn = sqlite3.connect(os.path.dirname(os.path.abspath(__file__)) + '/ip.db')
c = conn.cursor()

ips = []

for url in URLS:
    result = dns.resolver.resolve(url, 'A')

    for ipval in result:
        ips.append([ipval.to_text(), url, url])


if len(ips) > 0:
    c.executemany('INSERT INTO ip (ip, domain, last_seen) VALUES (?, ?, DateTime(\'now\')) ON CONFLICT (ip) DO UPDATE SET domain=?, last_seen=DateTime(\'now\')', ips)



# Build the command based on IPs seen in the number of seconds defined by PERIOD
arguments = [
    '/usr/sbin/networksetup',
    '-setadditionalroutes',
    SERVICE_NAME
]

now = datetime.now()
cutoff = datetime.fromtimestamp(now.timestamp() - PERIOD).isoformat(' ', 'seconds')


for row in c.execute('SELECT * FROM ip WHERE last_seen > ? ORDER BY ip', [cutoff]):
    arguments.append(row[0])
    arguments.append(MASK)
    arguments.append(GATEWAY)

conn.commit()
conn.close()

subprocess.run(arguments)

print('Process completed ' + str(now.isoformat(' ', 'minutes')))
